package com.manman.common.vo;

import java.io.Serializable;

/**
 *  VO 클래스
 * @author 박진만
 * @since 2016.10.20
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2016.10.20		박진만 		최초 생성
 * </pre>
 */
public class WebCrawlingVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7779953252636996410L;
	
	private String regNo;
	private int rank;
	private String title;
	private String artist;
	private String regYmd;
	private String category;
	
	private String thumbnail; //youtube thumbnail url
	private String videoId; //youtube video key
	private String videoTitle; //youtube video title
	private String regDt;	
	
	@Override
	public int hashCode(){
		return (this.videoId.hashCode());
	}

	@Override
	public boolean equals(Object obj){
		if(obj instanceof WebCrawlingVO){
			WebCrawlingVO temp = (WebCrawlingVO) obj;
			if(this.videoId.equals(temp.videoId)){
				return true;
			}
		}
		return false;
	}
	
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getRegYmd() {
		return regYmd;
	}
	public void setRegYmd(String regYmd) {
		this.regYmd = regYmd;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public String getVideoTitle() {
		return videoTitle;
	}
	public void setVideoTitle(String videoTitle) {
		this.videoTitle = videoTitle;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
}
