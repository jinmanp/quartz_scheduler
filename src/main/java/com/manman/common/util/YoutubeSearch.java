package com.manman.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;

/**
 * Youtube V3 API Search 클래스
 * @author 박진만
 * @since 2018.04.19
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2018.04.19		박진만 		최초 생성
 * </pre>
 */
public class YoutubeSearch {
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/** Global instance properties filename. */
	private static String PROPERTIES_FILENAME = "youtube.properties";
	
	 /** Global instance of the HTTP transport. */
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	
	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	
	 /** Global instance of the max number of videos we want returned (50 = upper limit per page). */
	private static final long NUMBER_OF_VIDEOS_RETURNED = 1;
	
	/** Global instance of Youtube object to make all API requests. */
	private static YouTube youtube;
	
	/**
	 * Initializes YouTube object to search for videos on YouTube (Youtube.Search.List). The program
	 * then prints the names and thumbnails of each of the videos (only first 50 videos).
	 * @param args command line args.
	 */
	public SearchResult search(String searchStr) {
		
		SearchResult singleVideo = new SearchResult();
		
		// Read the developer key from youtube.properties
		Properties properties = new Properties();
		
		try {
			InputStream in = YoutubeSearch.class.getResourceAsStream("/" + PROPERTIES_FILENAME);
			properties.load(in);
		} catch (IOException e) {
			logger.error("There was an error reading " + PROPERTIES_FILENAME + ": " + e.getCause()+ " : " + e.getMessage());
			System.exit(1);
		}
		
		try {
			/*
			 * The YouTube object is used to make all API requests. The last argument is required, but
			 * because we don't need anything initialized when the HttpRequest is initialized, we override
			 * the interface and provide a no-op function.
			 */
			youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, new HttpRequestInitializer() {
				public void initialize(HttpRequest request) throws IOException {}
				}).setApplicationName("youtube-cmdline-search-sample").build();
			
			// Get query term from user.
			String queryTerm = searchStr;
			
			YouTube.Search.List search = youtube.search().list("id,snippet");
			//YouTube.Search.List search = youtube.search().list("id");
			
			//YouTube.Videos.List search = youtube.videos().list("id");
			
			/*
			 * It is important to set your developer key from the Google Developer Console for
			 * non-authenticated requests (found under the API Access tab at this link:
			 * code.google.com/apis/). This is good practice and increased your quota.
			 */
			String apiKey = properties.getProperty("youtube.apikey");
			search.setKey(apiKey);
			search.setQ(queryTerm);
			
			logger.debug("\n=============================================================");
			logger.debug(
		        "   First " + NUMBER_OF_VIDEOS_RETURNED + " videos for search on \"" + queryTerm + "\".");
			logger.debug("=============================================================\n");
			
			/*
			 * We are only searching for videos (not playlists or channels). If we were searching for
			 * more, we would add them as a string like this: "video,playlist,channel".
			 */
			search.setType("video");
			
			/*
			 * This method reduces the info returned to only the fields we need and makes calls more
			 * efficient.
			 */
			search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
			//search.setFields("items(id/videoId)");
			search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);
			
			SearchListResponse searchResponse = search.execute();
			
			List<SearchResult> searchResultList = searchResponse.getItems();
			
			if (searchResultList.size() > 0) {
				
				singleVideo = searchResultList.get(0);
				//ResourceId rId = singleVideo.getId();
				
				// Double checks the kind is video.
				/*if (rId.getKind().equals("youtube#video")) {
					Thumbnail thumbnail = (Thumbnail) singleVideo.getSnippet().getThumbnails().get("default");
					//logger.debug(" Video Id : " + rId.getVideoId());
					//logger.debug(" Title : " + singleVideo.getSnippet().getTitle());
					//logger.debug(" Thumbnail : " + thumbnail.getUrl());
					//logger.debug("-------------------------------------------------------------");
				}*/				
				
			}else {				
				logger.info("YoutubeSearch There aren't any results for your query.");
			}
			
			return singleVideo;
			
		} catch (GoogleJsonResponseException e) {
			logger.error("YoutubeSearch There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
			return singleVideo;
		} catch (IOException e) {
			logger.error("YoutubeSearch There was an IO error: " + e.getCause() + " : " + e.getMessage());
			return singleVideo;
		} catch (Throwable t) {
			logger.error("YoutubeSearch There was a Throwable: " + t.getCause() + " : " + t.getMessage());
			return singleVideo;
		}
		
	}	


}
