package com.manman.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.stereotype.Component;

@Component
public class KeyGeneration {
	
	private static final Logger logger = LoggerFactory.getLogger(KeyGeneration.class);

	public String getKey(StepExecution stepExecution) {
		return stepExecution.getJobParameters().getString("currentTime") + "_"
				+ stepExecution.getJobParameters().getString("jobId") + "_"
				+ stepExecution.getJobParameters().getString("pgId") + "_";
	}

	public String getKey(JobExecution jobExecution) {
		return jobExecution.getJobParameters().getString("currentTime") + "_"
				+ jobExecution.getJobParameters().getString("jobId") + "_"
				+ jobExecution.getJobParameters().getString("pgId") + "_";
	}
	
}
