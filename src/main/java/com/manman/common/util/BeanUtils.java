package com.manman.common.util;

import org.springframework.context.ApplicationContext;

public class BeanUtils {
	
	public static <T> T getBean(Class<T> type) {
		ApplicationContext applicationContext = ApplicationContextProvider.getApplicationContext();
		return (T) applicationContext.getBean(type);
	}
	
}
