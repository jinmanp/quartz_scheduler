package com.manman.common.util;

import org.quartz.CronScheduleBuilder;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QuartzScheduleHandler {
	
	private static Logger logger = LoggerFactory.getLogger(QuartzScheduleHandler.class);

	private static final String QUARTZ_JOB_NAME = "schedulerModuleJob";

	private static final String QUARTZ_JOB_GRUOP_NAME = "schedulerModuleJobGroup";

	private static final String QUARTZ_JOB_TRIGGER_GROUP_NAME = "schedulerModuleTriggerGroup";

	private final Scheduler scheduler;

	private final String jobId;

	private final String pgId;

	private final String cronSchedule;

	private final String triggerName;

	public QuartzScheduleHandler(Scheduler scheduler, String jobId, String pgId, String cronSchedule) {
		this.scheduler = scheduler;
		this.jobId = jobId;
		this.pgId = pgId;
		this.cronSchedule = cronSchedule;
		this.triggerName = jobId + "_" + pgId;
	}

	public void setCronSchedule() throws Exception {
		scheduler.scheduleJob(getCronTrigger());
	}

	public void modifyCronSchedule() throws Exception {
		scheduler.rescheduleJob(TriggerKey.triggerKey(triggerName, QUARTZ_JOB_TRIGGER_GROUP_NAME),
				getCronTrigger());
	}

	public void setSimpleSchedule() throws Exception {
		scheduler.scheduleJob(getSimpleTrigger());
	}

	public void modifySimpleSchedule() throws Exception {
		scheduler.rescheduleJob(TriggerKey.triggerKey(triggerName, QUARTZ_JOB_TRIGGER_GROUP_NAME),
				getSimpleTrigger());
	}

	public void deleteSchdule() throws Exception {
		scheduler.unscheduleJob(TriggerKey.triggerKey(triggerName, QUARTZ_JOB_TRIGGER_GROUP_NAME));
	}

	public Boolean isExistSchedule() throws Exception {
		Boolean isExist = Boolean.valueOf(
				scheduler.checkExists(TriggerKey.triggerKey(triggerName, QUARTZ_JOB_TRIGGER_GROUP_NAME)));
		return isExist;
	}

	private Trigger getCronTrigger() throws Exception {
		return TriggerBuilder.newTrigger()
				.forJob(QUARTZ_JOB_NAME, QUARTZ_JOB_GRUOP_NAME)
				.withIdentity(triggerName, QUARTZ_JOB_TRIGGER_GROUP_NAME)
				.usingJobData("jobId", jobId)
				.usingJobData("pgId", pgId)
				.withSchedule((ScheduleBuilder) CronScheduleBuilder.cronSchedule(cronSchedule))
				.build();
	}

	private Trigger getSimpleTrigger() throws Exception {
		return TriggerBuilder.newTrigger()
				.forJob(QUARTZ_JOB_NAME, QUARTZ_JOB_GRUOP_NAME)
				.withIdentity(triggerName, QUARTZ_JOB_TRIGGER_GROUP_NAME)
				.usingJobData("jobId", jobId)
				.usingJobData("pgId", pgId)
				.withSchedule((ScheduleBuilder) SimpleScheduleBuilder.simpleSchedule())
				.build();
	}

}
