package com.manman.common.util;

import com.google.common.collect.Maps;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DataShareBean<T> {
	
	private static final Logger logger = LoggerFactory.getLogger(DataShareBean.class);

	private final Map<String, T> shareDataMap = Maps.newConcurrentMap();

	public void putData(String key, T data) {
		if (this.shareDataMap == null) {
			logger.error("Map is not initialize.");
			return;
		}
		this.shareDataMap.put(key, data);
	}

	public void putData(long key, T data) {
		if (this.shareDataMap == null) {
			logger.error("Map is not initialize.");
			return;
		}
		this.shareDataMap.put(String.valueOf(key), data);
	}

	public void removeData(String key) {
		if (this.shareDataMap == null) {
			logger.error("Map is not initialize.");
			return;
		}
		this.shareDataMap.remove(key);
	}

	public void removeData(long key) {
		if (this.shareDataMap == null) {
			logger.error("Map is not initialize.");
			return;
		}
		this.shareDataMap.remove(String.valueOf(key));
	}

	public T getData(String key) {
		if (this.shareDataMap == null)
			return null;
		return this.shareDataMap.get(key);
	}

	public T getData(long key) {
		if (this.shareDataMap == null)
			return null;
		return this.shareDataMap.get(String.valueOf(key));
	}

	public int getSize() {
		if (this.shareDataMap == null) {
			logger.error("Map is not initialize.");
			return 0;
		}
		return this.shareDataMap.size();
	}

	public void clear() {
		logger.debug("DataShareBean clear() before size() : " + this.shareDataMap.size());
		this.shareDataMap.clear();
		logger.debug("DataShareBean clear() after size() : " + this.shareDataMap.size());
	}

	public void removeJobData(String currentTime, String jobId, String pgId) {
		
		logger.debug("removeJobData before size() : " + this.shareDataMap.size());
		
		for (Map.Entry<String, T> entry : this.shareDataMap.entrySet()) {
			if (((String) entry.getKey()).contains(currentTime + "_" + jobId + "_" + pgId))
				removeData(entry.getKey());
		}
		
		logger.debug("removeJobData after size() : " + this.shareDataMap.size());
		
	}
	
}
