package com.manman.common.config;

import javax.sql.DataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@MapperScan(basePackages = { "com.manman.**.mapper" }, sqlSessionFactoryRef = "dbSqlSessionFactory")
@EnableTransactionManagement
@PropertySource({ "classpath:/application.properties" })
public class BatchDbConfig {
	
	@Autowired
	Environment environment;

	@Primary
	@Bean(name = { "batchDataSource" })
	@ConfigurationProperties(prefix = "spring.db.datasource")
	public DataSource batchDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(this.environment.getProperty("spring.db.datasource.driverClassName"));
		dataSource.setUrl(this.environment.getProperty("spring.db.datasource.url"));
		dataSource.setUsername(this.environment.getProperty("spring.db.datasource.username"));
		dataSource.setPassword(this.environment.getProperty("spring.db.datasource.password"));
		return (DataSource) dataSource;
	}

	@Primary
	@Bean(name = { "dbSqlSessionFactory" })
	public SqlSessionFactory dbSqlSessionFactory(@Qualifier("batchDataSource") DataSource batchDataSource,
			ApplicationContext applicationContext) throws Exception {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(batchDataSource);
		sqlSessionFactory.setMapperLocations(
				(new PathMatchingResourcePatternResolver()).getResources("classpath:/mappers/**/*_SQL.xml"));
		Resource myBatisConfig = (new PathMatchingResourcePatternResolver())
				.getResource("classpath:mybatis-config.xml");
		sqlSessionFactory.setConfigLocation(myBatisConfig);
		return sqlSessionFactory.getObject();
	}

	@Primary
	@Bean(name = { "dbSqlSessionTemplate" })
	public SqlSessionTemplate dbSqlSessionTemplate(SqlSessionFactory dbSqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(dbSqlSessionFactory);
	}
	
}
