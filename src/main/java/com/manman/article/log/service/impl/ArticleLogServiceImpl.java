package com.manman.article.log.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.manman.article.log.mapper.ArticleLogMapper;
import com.manman.article.log.service.ArticleLogService;
import com.manman.article.log.vo.ArticleLogVO;

/**
 * Log ServiceImpl 클래스
 * @author 박진만
 * @since 2018.04.19
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2018.04.19		박진만 		최초 생성
 * </pre>
 */
@Service("ArticleLogService")
@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
public class ArticleLogServiceImpl implements ArticleLogService{
	
	final private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ArticleLogMapper articleLogMapper;
	
	/**
	 * Log 입력
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false, rollbackForClassName="Exception")
	public int insertArticleLog(ArticleLogVO articleLogVO) {
		
		int result = 0;
		
		try {
			
			result = articleLogMapper.insertArticleLog(articleLogVO);
			
		} catch (Exception e) {			
			logger.error("ArticleLogServiceImpl insertArticleLog Exception : " + e.getCause() + " : " + e.getMessage());
			return result;
		}
		
		return result;
	}	
	
}
