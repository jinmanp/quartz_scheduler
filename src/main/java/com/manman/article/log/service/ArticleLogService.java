package com.manman.article.log.service;

import com.manman.article.log.vo.ArticleLogVO;

/**
 * Log Servicel 클래스
 * @author 박진만
 * @since 2018.04.19
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2018.04.19		박진만 		최초 생성
 * </pre>
 */
public interface ArticleLogService {	
	
	/**
	 * Log 입력
	 */
	int insertArticleLog(ArticleLogVO ArticleLogVO);

}
