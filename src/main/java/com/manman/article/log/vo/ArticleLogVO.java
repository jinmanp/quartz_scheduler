package com.manman.article.log.vo;

import java.io.Serializable;

/**
 * LostArticleLogVO 클래스
 * @author 박진만
 * @since 2019.04.15
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.15		박진만 		최초 생성
 * </pre>
 */

public class ArticleLogVO implements Serializable{
	
	private static final long serialVersionUID = 2454683681416475530L;
	
	private int logSeq;
	private String progId;
	private String progNm;
	private String result;
	private int procCnt;
	private String errMsg;
	
	public int getLogSeq() {
		return logSeq;
	}
	public void setLogSeq(int logSeq) {
		this.logSeq = logSeq;
	}
	public String getProgId() {
		return progId;
	}
	public void setProgId(String progId) {
		this.progId = progId;
	}
	public String getProgNm() {
		return progNm;
	}
	public void setProgNm(String progNm) {
		this.progNm = progNm;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getProcCnt() {
		return procCnt;
	}
	public void setProcCnt(int procCnt) {
		this.procCnt = procCnt;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
}
