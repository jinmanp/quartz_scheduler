package com.manman.article.log.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.manman.article.log.vo.ArticleLogVO;

@Mapper
@Repository
public interface ArticleLogMapper {
	
	public int insertArticleLog(ArticleLogVO ArticleLogVO) throws Exception;

}
