package com.manman.article.lostarticle.vo;

import java.io.Serializable;

/**
 * LostArticle VO 클래스
 * @author 박진만
 * @since 2019.04.15
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.15		박진만 		최초 생성
 * </pre>
 */
public class LostArticleVO implements Serializable {
	
	private static final long serialVersionUID = -3169366482553824251L;	
	
	private String atcId;
	private String lstFilePathImg;
	private String lstHor;
	private String lstLctNm;
	private String lstLctCd;
	private String lstPlace;
	private String lstPlaceSeNm;	
	private String lstPrdtNm;
	private String lstSbjt;
	private String lstSteNm;	
	private String lstYmd;
	private String orgId;
	private String orgNm;
	private String prdtClNm;
	private String prdtClCd01;
	private String prdtClCd02;
	private String tel;
	private String uniq;
	private String regYmd;
	private Boolean searchRegYmd;
	
	private String startYmd;
	private String endYmd;
	private String searchNm;
	private String p;
	
	public String getAtcId() {
		return atcId;
	}
	public void setAtcId(String atcId) {
		this.atcId = atcId;
	}
	public String getLstFilePathImg() {
		return lstFilePathImg;
	}
	public void setLstFilePathImg(String lstFilePathImg) {
		this.lstFilePathImg = lstFilePathImg;
	}
	public String getLstHor() {
		return lstHor;
	}
	public void setLstHor(String lstHor) {
		this.lstHor = lstHor;
	}
	public String getLstLctNm() {
		return lstLctNm;
	}
	public void setLstLctNm(String lstLctNm) {
		this.lstLctNm = lstLctNm;
	}
	public String getLstLctCd() {
		return lstLctCd;
	}
	public void setLstLctCd(String lstLctCd) {
		this.lstLctCd = lstLctCd;
	}
	public String getLstPlace() {
		return lstPlace;
	}
	public void setLstPlace(String lstPlace) {
		this.lstPlace = lstPlace;
	}
	public String getLstPlaceSeNm() {
		return lstPlaceSeNm;
	}
	public void setLstPlaceSeNm(String lstPlaceSeNm) {
		this.lstPlaceSeNm = lstPlaceSeNm;
	}
	public String getLstPrdtNm() {
		return lstPrdtNm;
	}
	public void setLstPrdtNm(String lstPrdtNm) {
		this.lstPrdtNm = lstPrdtNm;
	}
	public String getLstSbjt() {
		return lstSbjt;
	}
	public void setLstSbjt(String lstSbjt) {
		this.lstSbjt = lstSbjt;
	}
	public String getLstSteNm() {
		return lstSteNm;
	}
	public void setLstSteNm(String lstSteNm) {
		this.lstSteNm = lstSteNm;
	}
	public String getLstYmd() {
		return lstYmd;
	}
	public void setLstYmd(String lstYmd) {
		this.lstYmd = lstYmd;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgNm() {
		return orgNm;
	}
	public void setOrgNm(String orgNm) {
		this.orgNm = orgNm;
	}
	public String getPrdtClNm() {
		return prdtClNm;
	}
	public void setPrdtClNm(String prdtClNm) {
		this.prdtClNm = prdtClNm;
	}	
	public String getPrdtClCd01() {
		return prdtClCd01;
	}
	public void setPrdtClCd01(String prdtClCd01) {
		this.prdtClCd01 = prdtClCd01;
	}
	public String getPrdtClCd02() {
		return prdtClCd02;
	}
	public void setPrdtClCd02(String prdtClCd02) {
		this.prdtClCd02 = prdtClCd02;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getUniq() {
		return uniq;
	}
	public void setUniq(String uniq) {
		this.uniq = uniq;
	}
	public String getStartYmd() {
		return startYmd;
	}
	public void setStartYmd(String startYmd) {
		this.startYmd = startYmd;
	}
	public String getEndYmd() {
		return endYmd;
	}
	public void setEndYmd(String endYmd) {
		this.endYmd = endYmd;
	}
	public String getSearchNm() {
		return searchNm;
	}
	public void setSearchNm(String searchNm) {
		this.searchNm = searchNm;
	}
	public String getRegYmd() {
		return regYmd;
	}
	public void setRegYmd(String regYmd) {
		this.regYmd = regYmd;
	}
	public Boolean getSearchRegYmd() {
		return searchRegYmd;
	}
	public void setSearchRegYmd(Boolean searchRegYmd) {
		this.searchRegYmd = searchRegYmd;
	}
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
	
}
