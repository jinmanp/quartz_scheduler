package com.manman.article.lostarticle.service;

import java.util.List;

import com.manman.article.log.vo.ArticleLogVO;
import com.manman.article.lostarticle.vo.LostArticleVO;

/**
 * LostArticle Servicel 클래스
 * @author 박진만
 * @since 2019.04.15
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.15		박진만 		최초 생성
 * </pre>
 */
public interface LostArticleService {	

	/**
	 * 분실물 리스트 조회
	 */
	List<LostArticleVO> selectLostArticleList(LostArticleVO lostArticleVO) throws Exception;
	
	int selectCountLostArticleList(LostArticleVO lostArticleVO) throws Exception;	
	
	/**
	 * 분실물 목록 등록
	 */
	ArticleLogVO insertLostArticleList(String lstYmd);

}
