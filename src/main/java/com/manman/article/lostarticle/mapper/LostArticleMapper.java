package com.manman.article.lostarticle.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.manman.article.lostarticle.vo.LostArticleVO;

@Mapper
@Repository
public interface LostArticleMapper {
	
	public List<LostArticleVO> selectLostArticleList(LostArticleVO lostArticleVO) throws Exception;
	
	public int selectCountLostArticleList(LostArticleVO lostArticleVO) throws Exception;
	
	public int selectCheckLostArticle(LostArticleVO lostArticleVO) throws Exception;
	
	public int insertLostArticleList(List<LostArticleVO> resultList) throws Exception;

}
