package com.manman.article.foundarticle.mapper;

import com.manman.article.foundarticle.vo.FoundArticleVO;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface FoundArticleMapper {  
  
  	public List<FoundArticleVO> selectFoundArticleList(FoundArticleVO foundArticleVO) throws Exception;
	
	public int selectCountFoundArticleList(FoundArticleVO foundArticleVO) throws Exception;
	
	public int selectCheckFoundArticle(FoundArticleVO foundArticleVO) throws Exception;
	
	public int insertFoundArticleList(List<FoundArticleVO> resultList) throws Exception;
	
	public int updateFoundArticleList(List<FoundArticleVO> updatetList) throws Exception;

	//public int updateFoundArticle(FoundArticleVO foundArticleVO) throws Exception;

	public List<FoundArticleVO> selectFoundArticleAllList(FoundArticleVO foundArticleVO1) throws Exception;

	public int insertFoundArticleListSeoul(List<FoundArticleVO> resultList) throws Exception;
	
	public int updateFoundArticleListSeoul(List<FoundArticleVO> updatetList) throws Exception;

	public FoundArticleVO selectFoundArticle(FoundArticleVO foundArticleVO) throws Exception;
  
}
