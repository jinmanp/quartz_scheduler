package com.manman.article.foundarticle.service;

import com.manman.article.foundarticle.vo.FoundArticleVO;
import java.util.List;

public interface FoundArticleService {
	
  List<FoundArticleVO> selectFoundArticleList(FoundArticleVO paramFoundArticleVO) throws Exception;
  
  int selectCountFoundArticleList(FoundArticleVO paramFoundArticleVO) throws Exception;
  
  void insertFoundArticleList(String paramString1, String paramString2);
  
  void sendFcm();
  
  void insertFoundArticleListSeoul(String paramString);
  
}
