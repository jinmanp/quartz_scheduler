package com.manman.article.foundarticle.vo;

import java.io.Serializable;

/**
 * FoundArticle VO 클래스
 * @author 박진만
 * @since 2019.04.18
 * @version 1.0.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *  수정일			수정자			수정내용
 *  -------    		--------    ---------------------------
 *  2019.04.18		박진만 		최초 생성
 * </pre>
 */
public class FoundArticleVO implements Serializable {
	
	private static final long serialVersionUID = -6803772186330228567L;
	
	private String atcId;
	private String csteSteNm;
	private String depPlace;
	private String fdFilePathImg;
	private String fdHor;
	private String fdLctNm;
	private String fdLctCd;	
	private String fdPlace;	
	private String fdPrdtNm;
	private String fdSn;	
	private String fdYmd;
	private String fndKeepOrgn;
	private String orgId;
	private String orgNm;
	private String prdtClNm;
	private String prdtClCd01;
	private String prdtClCd02;
	private String tel;
	private String uniq;
	private String regYmd;
	private Boolean searchRegYmd;
	
	private String startYmd;
	private String endYmd;
	private String searchNm;
	private String p;
	
	public String getAtcId() {
		return atcId;
	}
	public void setAtcId(String atcId) {
		this.atcId = atcId;
	}
	public String getCsteSteNm() {
		return csteSteNm;
	}
	public void setCsteSteNm(String csteSteNm) {
		this.csteSteNm = csteSteNm;
	}
	public String getDepPlace() {
		return depPlace;
	}
	public void setDepPlace(String depPlace) {
		this.depPlace = depPlace;
	}
	public String getFdFilePathImg() {
		return fdFilePathImg;
	}
	public void setFdFilePathImg(String fdFilePathImg) {
		this.fdFilePathImg = fdFilePathImg;
	}
	public String getFdHor() {
		return fdHor;
	}
	public void setFdHor(String fdHor) {
		this.fdHor = fdHor;
	}
	public String getFdLctNm() {
		return fdLctNm;
	}
	public void setFdLctNm(String fdLctNm) {
		this.fdLctNm = fdLctNm;
	}
	public String getFdLctCd() {
		return fdLctCd;
	}
	public void setFdLctCd(String fdLctCd) {
		this.fdLctCd = fdLctCd;
	}
	public String getFdPlace() {
		return fdPlace;
	}
	public void setFdPlace(String fdPlace) {
		this.fdPlace = fdPlace;
	}
	public String getFdPrdtNm() {
		return fdPrdtNm;
	}
	public void setFdPrdtNm(String fdPrdtNm) {
		this.fdPrdtNm = fdPrdtNm;
	}
	public String getFdSn() {
		return fdSn;
	}
	public void setFdSn(String fdSn) {
		this.fdSn = fdSn;
	}
	public String getFdYmd() {
		return fdYmd;
	}
	public void setFdYmd(String fdYmd) {
		this.fdYmd = fdYmd;
	}
	public String getFndKeepOrgn() {
		return fndKeepOrgn;
	}
	public void setFndKeepOrgn(String fndKeepOrgn) {
		this.fndKeepOrgn = fndKeepOrgn;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgNm() {
		return orgNm;
	}
	public void setOrgNm(String orgNm) {
		this.orgNm = orgNm;
	}
	public String getPrdtClNm() {
		return prdtClNm;
	}
	public void setPrdtClNm(String prdtClNm) {
		this.prdtClNm = prdtClNm;
	}
	public String getPrdtClCd01() {
		return prdtClCd01;
	}
	public void setPrdtClCd01(String prdtClCd01) {
		this.prdtClCd01 = prdtClCd01;
	}
	public String getPrdtClCd02() {
		return prdtClCd02;
	}
	public void setPrdtClCd02(String prdtClCd02) {
		this.prdtClCd02 = prdtClCd02;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getUniq() {
		return uniq;
	}
	public void setUniq(String uniq) {
		this.uniq = uniq;
	}
	public String getStartYmd() {
		return startYmd;
	}
	public void setStartYmd(String startYmd) {
		this.startYmd = startYmd;
	}
	public String getEndYmd() {
		return endYmd;
	}
	public void setEndYmd(String endYmd) {
		this.endYmd = endYmd;
	}
	public String getSearchNm() {
		return searchNm;
	}
	public void setSearchNm(String searchNm) {
		this.searchNm = searchNm;
	}
	public String getRegYmd() {
		return regYmd;
	}
	public void setRegYmd(String regYmd) {
		this.regYmd = regYmd;
	}
	public Boolean getSearchRegYmd() {
		return searchRegYmd;
	}
	public void setSearchRegYmd(Boolean searchRegYmd) {
		this.searchRegYmd = searchRegYmd;
	}
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
	
}
