package com.manman.batch.log.mapper;

import com.manman.batch.log.vo.BatchLogVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface BatchLogMapper {
	
	void insertBatchLog(BatchLogVO paramBatchLogVO) throws Exception;

	void updateBatchLog(BatchLogVO paramBatchLogVO) throws Exception;
	
}
