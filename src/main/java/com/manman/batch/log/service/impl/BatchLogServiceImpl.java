package com.manman.batch.log.service.impl;

import com.manman.batch.log.mapper.BatchLogMapper;
import com.manman.batch.log.service.BatchLogService;
import com.manman.batch.log.vo.BatchLogVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("BatchLogService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class BatchLogServiceImpl implements BatchLogService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private BatchLogMapper batchLogMapper;

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = { "Exception" })
	public void insertBatchLog(BatchLogVO batchLogVO) {
		
		try {
			this.batchLogMapper.insertBatchLog(batchLogVO);
		} catch (Exception e) {
			this.logger
					.error("BatchLogServiceImpl insertBatchLog Exception : " + e.getCause() + " : " + e.getMessage());
		}
		
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackForClassName = { "Exception" })
	public void updateBatchLog(BatchLogVO batchLogVO) {
		
		try {
			this.batchLogMapper.updateBatchLog(batchLogVO);
		} catch (Exception e) {
			this.logger
					.error("BatchLogServiceImpl updateBatchLog Exception : " + e.getCause() + " : " + e.getMessage());
		}
		
	}
}
