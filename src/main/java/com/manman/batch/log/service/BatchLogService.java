package com.manman.batch.log.service;

import com.manman.batch.log.vo.BatchLogVO;

public interface BatchLogService {
	
	void insertBatchLog(BatchLogVO paramBatchLogVO);

	void updateBatchLog(BatchLogVO paramBatchLogVO);
	
}
