package com.manman.batch.log.vo;

import java.io.Serializable;

public class BatchLogVO implements Serializable {
	
	private static final long serialVersionUID = -2476382719020495880L;

	private int logSeq;

	private String jobId;

	private String progId;

	private String currentTm;

	private String procAt;

	private String errMsg;

	public int getLogSeq() {
		return this.logSeq;
	}

	public void setLogSeq(int logSeq) {
		this.logSeq = logSeq;
	}

	public String getJobId() {
		return this.jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getProgId() {
		return this.progId;
	}

	public void setProgId(String progId) {
		this.progId = progId;
	}

	public String getCurrentTm() {
		return this.currentTm;
	}

	public void setCurrentTm(String currentTm) {
		this.currentTm = currentTm;
	}

	public String getProcAt() {
		return this.procAt;
	}

	public void setProcAt(String procAt) {
		this.procAt = procAt;
	}

	public String getErrMsg() {
		return this.errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
}
