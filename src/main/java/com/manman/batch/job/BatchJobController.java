package com.manman.batch.job;

import com.manman.common.util.BeanUtils;
import com.manman.common.util.DataShareBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BatchJobController {
	
	private static final Logger logger = LoggerFactory.getLogger(BatchJobController.class);

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private Job batchJob;

	public void runBatchJob(JobParameters params) {
		
		try {
			this.jobLauncher.run(this.batchJob, params);
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("BatchJobController JobExecutionAlreadyRunningException : " + e.toString());
			logger.error("BatchJobController runLtsugJob info : " + this.batchJob.toString());
			logger.error("BatchJobController JobParameters info : " + params.toString());
		} catch (JobRestartException e) {
			logger.error("BatchJobController JobRestartException : " + e.toString());
			logger.error("BatchJobController runLtsugJob info : " + this.batchJob.toString());
			logger.error("BatchJobController JobParameters info : " + params.toString());
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("BatchJobController JobInstanceAlreadyCompleteException : " + e.toString());
			logger.error("BatchJobController runLtsugJob info : " + this.batchJob.toString());
			logger.error("BatchJobController JobParameters info : " + params.toString());
		} catch (JobParametersInvalidException e) {
			logger.error("BatchJobController JobParametersInvalidException : " + e.toString());
			logger.error("BatchJobController runLtsugJob info : " + this.batchJob.toString());
			logger.error("BatchJobController JobParameters info : " + params.toString());
		} catch (Exception e) {
			logger.error("BatchJobController Exception : " + e.getMessage());
			logger.error("BatchJobController runLtsugJob info : " + this.batchJob.toString());
			logger.error("BatchJobController JobParameters info : " + params.toString());
		} finally {
			DataShareBean<Object> dataShareBean = (DataShareBean<Object>) BeanUtils.getBean(DataShareBean.class);
			dataShareBean.removeJobData(params.getString("currentTime"), params.getString("jobId"),
					params.getString("pgId"));
			logger.debug("BatchJobController runBatchJob " + params.getString("currentTime") + "_"
					+ params.getString("jobId") + "_" + params.getString("pgId") + " finally call");
		}
		
	}
	
}
