package com.manman.batch.job;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.manman.batch.decider.BatchDecider;
import com.manman.batch.step.config.FoundArticleSendFcmStepConfig;
import com.manman.batch.step.config.FoundArticleStepConfig;
import com.manman.batch.step.config.LostArticleStepConfig;
import com.manman.batch.step.config.StartStepConfig;
import com.manman.batch.step.config.Top100MusicStepConfig;
import com.manman.batch.step.config.TrotStepConfig;

@EnableBatchProcessing
@Configuration
public class BatchJobConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(BatchJobConfig.class);

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private BatchDecider batchDecider;

	@Autowired
	private StartStepConfig startStepConfig;

	@Autowired
	private FoundArticleStepConfig foundArticleStepConfig;
	
	@Autowired
	private FoundArticleSendFcmStepConfig foundArticleSendFcmStepConfig;

	@Autowired
	private LostArticleStepConfig lostArticleStepConfig;

	@Autowired
	private Top100MusicStepConfig top100MusicStepConfig;

	@Autowired
	private TrotStepConfig trotStepConfig;

	@Autowired
	private BatchJobListener batchJobListener;

	@Bean
	public Job batchJob() throws IOException, InvocationTargetException, SQLException, Exception {
		
		logger.debug("init batchJob");
		
		FlowBuilder<Flow> foundArticleFlowBuilder = new FlowBuilder<Flow>("foundArticle");
		Flow foundArticleFlow = foundArticleFlowBuilder
				.start(foundArticleStepConfig.foundArticleStep())
				.next(foundArticleSendFcmStepConfig.foundArticleSendFcmStep())
				.end();
		
		FlowBuilder<Flow> lostArticleFlowBuilder = new FlowBuilder<Flow>("lostArticle");
		Flow lostArticleFlow = lostArticleFlowBuilder
				.start(lostArticleStepConfig.lostArticleStep())
				.end();
		
		FlowBuilder<Flow> top100MusicFlowBuilder = new FlowBuilder<Flow>("top100Music");
		Flow top100MusicFlow = top100MusicFlowBuilder
				.start(top100MusicStepConfig.top100MusicStep())
				.end();
		
		FlowBuilder<Flow> trotFlowBuilder = new FlowBuilder<Flow>("trot");
		Flow trotFlow = trotFlowBuilder
				.start(trotStepConfig.trotStep())
				.end();
		
		return jobBuilderFactory.get("batchJob")
				.listener(batchJobListener)
				.start(startStepConfig.startStep())
				.next(batchDecider)				
				.on("foundArticle")
				.to(foundArticleFlow)
				.from(batchDecider)
				.on("lostArticle")
				.to(lostArticleFlow)
				.from(batchDecider)
				.on("top100Music")
				.to(top100MusicFlow)
				.from(batchDecider)
				.on("trot")
				.to(trotFlow)
				.from(batchDecider)
				.on("endJob")
				.end()
				.end()
				.build();
				
	}
}
