package com.manman.batch.job;

import com.manman.batch.log.service.BatchLogService;
import com.manman.batch.log.vo.BatchLogVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BatchJobListener implements JobExecutionListener {
	
	private static final Logger logger = LoggerFactory.getLogger(BatchJobListener.class);

	@Autowired
	private JobOperator jobOperator;

	@Autowired
	private BatchLogService batchLogService;

	private Thread thread;

	public void afterJob(JobExecution jobExecution) {
		
		logger.debug("BatchJobListener afterJob start");
		
		List<Throwable> exceptions = jobExecution.getAllFailureExceptions();
		
		try {
			
			if (!exceptions.isEmpty()) {
				
				String errMsg = null;
				
				for (Throwable th : exceptions) {
					logger.error("LtsugJobListener afterJob Throwable exception : " + th);
					errMsg = errMsg + th + "\n";
				}
				
				BatchLogVO batchLogVO = new BatchLogVO();
				batchLogVO.setJobId(jobExecution.getJobParameters().getString("jobId"));
				batchLogVO.setProgId(jobExecution.getJobParameters().getString("pgId"));
				batchLogVO.setCurrentTm(jobExecution.getJobParameters().getString("currentTime"));
				batchLogVO.setProcAt("F");
				batchLogVO.setErrMsg(errMsg);
				
				this.batchLogService.updateBatchLog(batchLogVO);
				
			} else {
				
				BatchLogVO batchLogVO = new BatchLogVO();
				batchLogVO.setJobId(jobExecution.getJobParameters().getString("jobId"));
				batchLogVO.setProgId(jobExecution.getJobParameters().getString("pgId"));
				batchLogVO.setCurrentTm(jobExecution.getJobParameters().getString("currentTime"));
				batchLogVO.setProcAt("Y");
				batchLogVO.setErrMsg("");
				
				this.batchLogService.updateBatchLog(batchLogVO);
				
			}
			
		} catch (Exception e) {
			logger.error("BatchJobListener afterJob Exception : " + e.getMessage());
			throw new RuntimeException(e);
		}
		
		if (!this.thread.isAlive()) {
			logger.debug("BatchJobListener afterJob thread interrupt : " + jobExecution.getId());
			this.thread.interrupt();
			Runtime.getRuntime().removeShutdownHook(this.thread);
		}
		
		logger.debug("BatchJobListener afterJob end");
		
	}

	public void beforeJob(final JobExecution jobExecution) {
		
		Runtime.getRuntime().addShutdownHook(this.thread = new Thread() {
			public void run() {
				try {
					BatchJobListener.logger.debug("LtsugJobListener ShutdownHook start : " + jobExecution.getId()
							+ ", isRunning : " + jobExecution.isRunning() + ", ExitCode : "
							+ jobExecution.getExitStatus().getExitCode());
					if (jobExecution.isRunning() && !jobExecution.getExitStatus().getExitCode().equals("COMPLETED"))
						BatchJobListener.this.jobOperator.stop(jobExecution.getId().longValue());
					while (jobExecution.isRunning()) {
						BatchJobListener.logger
								.debug("LtsugJobListener waiting for job to stop... job id : " + jobExecution.getId());
						try {
							Thread.sleep(100L);
						} catch (Exception e) {
							BatchJobListener.logger
									.error("LtsugJobListener ShutdownHook Exception : " + e.getMessage());
						}
					}
					BatchJobListener.logger.debug("LtsugJobListener ShutdownHook end : " + jobExecution.getId()
							+ ", isRunning : " + jobExecution.isRunning() + ", ExitCode : "
							+ jobExecution.getExitStatus().getExitCode());
				} catch (Exception e) {
					BatchJobListener.logger.error("LtsugJobListener ShutdownHook Exception : " + e.getMessage());
				}
			}
		});
		
		logger.debug("LtsugJobListener beforeJob start");
		
		try {
			
			BatchLogVO batchLogVO = new BatchLogVO();
			batchLogVO.setJobId(jobExecution.getJobParameters().getString("jobId"));
			batchLogVO.setProgId(jobExecution.getJobParameters().getString("pgId"));
			batchLogVO.setCurrentTm(jobExecution.getJobParameters().getString("currentTime"));
			batchLogVO.setProcAt("P");
			
			this.batchLogService.insertBatchLog(batchLogVO);
			
		} catch (Exception e) {
			logger.error("BatchJobListener beforeJob Exception : " + e.getMessage());
			throw new RuntimeException(e);
		}
		
		logger.debug("LtsugJobListener beforeJob end");
		
	}
	
}
