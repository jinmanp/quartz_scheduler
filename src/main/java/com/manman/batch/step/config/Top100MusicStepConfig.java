package com.manman.batch.step.config;

import com.manman.top100music.crawling.service.WebCrawlingService;
import com.manman.top100music.log.service.LogService;
import com.manman.top100music.log.vo.LogVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Top100MusicStepConfig {
	private static final Logger logger = LoggerFactory.getLogger(Top100MusicStepConfig.class);

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private WebCrawlingService webCrawlingService;

	@Autowired
	private LogService logService;

	@Bean
	public Step top100MusicStep() {
		
		return stepBuilderFactory.get("top100MusicStep").tasklet((contribution, chunkContext) -> {
			
			logger.debug("top100MusicStepTasklet start!");
			
			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();
			String pgId = stepExecution.getJobParameters().getString("pgId");
			
			if (pgId != null) {
				
				if ("insertTop100ListRealtime".equals(pgId)) {
					logger.info("top100MusicStepTasklet insertTop100ListRealtime start");
					LogVO logVO = webCrawlingService.insertTop100ListRealtime();
					logService.insertLog(logVO);
					logger.info("top100MusicStepTasklet insertTop100ListRealtime is " + logVO.getResult());
				}
				
				if ("deleteTop100ListRealtime".equals(pgId)) {
					logger.info("top100MusicStepTasklet deleteTop100ListRealtime start");
					LogVO logVO = webCrawlingService.deleteTop100ListRealtime();
					logService.insertLog(logVO);
					logger.info("top100MusicStepTasklet deleteTop100ListRealtime is " + logVO.getResult());
				}
				
				if ("insertTop100ListDay".equals(pgId)) {
					logger.info("top100MusicStepTasklet insertTop100ListDay start");
					LogVO logVO = webCrawlingService.insertTop100ListDay();
					logService.insertLog(logVO);
					logger.info("top100MusicStepTasklet insertTop100ListDay is " + logVO.getResult());
				}
				
			} else {
				logger.error("top100MusicStepTasklet pgId is null");
			}
			
			logger.debug("top100MusicStepTasklet end");
			
			return RepeatStatus.FINISHED;
			
		}).build();
		
	}
	
}
