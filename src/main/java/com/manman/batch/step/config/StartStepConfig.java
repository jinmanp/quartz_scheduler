package com.manman.batch.step.config;

import com.manman.common.util.DataShareBean;
import com.manman.common.util.KeyGeneration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StartStepConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(StartStepConfig.class);

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private DataShareBean<Object> dataShareBean;

	@Autowired
	private KeyGeneration keyGeneration;

	@Bean
	public Step startStep() {
		return stepBuilderFactory.get("startStep").tasklet((contribution, chunkContext) -> {
			
			logger.debug("StartStepTasklet start");
			
			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();
			
			dataShareBean.putData(keyGeneration.getKey(stepExecution) + "job", Boolean.valueOf(false));
			
			logger.debug("StartStepTasklet end");
			
			return RepeatStatus.FINISHED;
			
		}).build();
		
	}
	
}
