package com.manman.batch.step.config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.manman.article.foundarticle.service.FoundArticleService;

@Configuration
public class FoundArticleStepConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(FoundArticleStepConfig.class);

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private FoundArticleService foundArticleService;

	@Bean
	public Step foundArticleStep() {
		
		return stepBuilderFactory.get("foundArticleStep").tasklet((contribution, chunkContext) -> {
			
			logger.debug("foundArticleStepTasklet start!");
			
			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();
			String currentYmd = stepExecution.getJobParameters().getString("currentYmd");
			ArrayList<String> ymdList = new ArrayList<>();
			
			ymdList.add(currentYmd);
			
			int i;
			
			for (i = 1; i <= 30; i++) {
				Calendar calendar = Calendar.getInstance();
				calendar.add(5, -i);
				String ymd = (new SimpleDateFormat("yyyyMMdd")).format(calendar.getTime());
				ymdList.add(ymd);
			}
			
			logger.info("foundArticleStepTasklet insertFoundArticleList police start");
			
			for (i = 0; i < ymdList.size(); i++) {
				foundArticleService.insertFoundArticleList(ymdList.get(i), "police");
			}
			
			logger.info("foundArticleStepTasklet insertFoundArticleList police end");
				
			logger.info("foundArticleStepTasklet insertFoundArticleList potal start");
			
			for (i = 0; i < ymdList.size(); i++) {
				foundArticleService.insertFoundArticleList(ymdList.get(i), "potal");
			}
			
			logger.info("foundArticleStepTasklet insertFoundArticleList potal end");
				
			logger.info("foundArticleStepTasklet insertFoundArticleList phone start");
			
			for (i = 0; i < ymdList.size(); i++) {
				foundArticleService.insertFoundArticleList(ymdList.get(i), "phone");
			}
				
			logger.info("foundArticleStepTasklet insertFoundArticleList phone end");
			
			logger.info("foundArticleStepTasklet insertFoundArticleListSeoul start");
			
			foundArticleService.insertFoundArticleListSeoul(ymdList.get(0));
			
			logger.info("foundArticleStepTasklet insertFoundArticleListSeoul end");
			
			logger.debug("foundArticleStepTasklet end");
			
			return RepeatStatus.FINISHED;
			
		}).build();
		
	}
	
}
