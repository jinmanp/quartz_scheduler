package com.manman.batch.step.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.manman.article.foundarticle.service.FoundArticleService;

@Configuration
public class FoundArticleSendFcmStepConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(FoundArticleSendFcmStepConfig.class);

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private FoundArticleService foundArticleService;

	@Bean
	public Step foundArticleSendFcmStep() {
		
		return stepBuilderFactory.get("foundArticleSendFcmStep").tasklet((contribution, chunkContext) -> {
			
			logger.debug("foundArticleSendFcmStepTasklet start!");
				
			logger.info("foundArticleStepTasklet sendFcm start");
			
			foundArticleService.sendFcm();
			
			logger.debug("foundArticleSendFcmStepTasklet end");
			
			return RepeatStatus.FINISHED;
			
		}).build();
		
	}
	
}