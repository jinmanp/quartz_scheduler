package com.manman.batch.step.config;

import com.manman.trot.crawling.service.TrotCrawlingService;
import com.manman.trot.log.service.TrotLogService;
import com.manman.trot.log.vo.TrotLogVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TrotStepConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(TrotStepConfig.class);

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private TrotCrawlingService trotCrawlingService;

	@Autowired
	private TrotLogService trotLogService;

	@Bean
	public Step trotStep() {
		
		return stepBuilderFactory.get("trotStep").tasklet((contribution, chunkContext) -> {
			
			logger.debug("trotStepTasklet start!");
			
			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();
			String pgId = stepExecution.getJobParameters().getString("pgId");
			
			if (pgId != null) {
				
				if ("insertMrTrot".equals(pgId)) {
					logger.info("trotStepTasklet insertMrTrot start");
					TrotLogVO trotLogVO = trotCrawlingService.insertMrTrot();
					trotLogService.insertTrotLog(trotLogVO);
					logger.info("trotStepTasklet insertMrTrot is " + trotLogVO.getResult());
					trotLogService.sendFcm();
				}
				
				if ("insertVoiceTrot".equals(pgId)) {
					logger.info("trotStepTasklet insertVoiceTrot start");
					TrotLogVO trotLogVO = trotCrawlingService.insertVoiceTrot();
					trotLogService.insertTrotLog(trotLogVO);
					logger.info("trotStepTasklet insertVoiceTrot is " + trotLogVO.getResult());
				}
				
			} else {
				logger.error("trotStepTasklet pgId is null");
			}
			
			logger.debug("trotStepTasklet end");
			
			return RepeatStatus.FINISHED;
			
		}).build();
		
	}
	
}
