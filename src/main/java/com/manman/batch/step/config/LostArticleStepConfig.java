package com.manman.batch.step.config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.manman.article.log.service.ArticleLogService;
import com.manman.article.log.vo.ArticleLogVO;
import com.manman.article.lostarticle.service.LostArticleService;

@Configuration
public class LostArticleStepConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(LostArticleStepConfig.class);

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private LostArticleService lostArticleService;

	@Autowired
	private ArticleLogService articleLogService;

	@Bean
	public Step lostArticleStep() {
		
		return stepBuilderFactory.get("lostArticleStep").tasklet((contribution, chunkContext) -> {
			
			logger.debug("lostArticleStepTasklet start!");
			
			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();
			ArticleLogVO articleLogVO = new ArticleLogVO();
			String currentYmd = stepExecution.getJobParameters().getString("currentYmd");
			ArrayList<String> ymdList = new ArrayList<>();
			
			ymdList.add(currentYmd);
			
			int i;
			
			for (i = 1; i <= 30; i++) {
				Calendar calendar = Calendar.getInstance();
				calendar.add(5, -i);
				String ymd = (new SimpleDateFormat("yyyyMMdd")).format(calendar.getTime());
				ymdList.add(ymd);
			}
			
			for (i = 0; i < ymdList.size(); i++) {
				articleLogVO = lostArticleService.insertLostArticleList(ymdList.get(i));
				articleLogService.insertArticleLog(articleLogVO);
				logger.info("lostArticleStepTasklet insertLostArticleList is " + articleLogVO.getResult());
			}
			
			logger.debug("lostArticleStepTasklet end");
			
			return RepeatStatus.FINISHED;
			
		}).build();
		
	}
	
}
