package com.manman.batch.config;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.ListableJobLocator;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.configuration.support.MapJobRegistry;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BatchConfig extends DefaultBatchConfigurer {

	@Autowired
	private DataSource batchDataSource;

	protected JobRepository createJobRepository()
			throws IOException, InvocationTargetException, SQLException, Exception {

		JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
		factory.setDatabaseType("POSTGRES");
		factory.setDataSource(this.batchDataSource);
		factory.setTransactionManager(getTransactionManager());
		factory.setIsolationLevelForCreate("ISOLATION_REPEATABLE_READ");
		factory.setTablePrefix("batch.BATCH_");
		factory.afterPropertiesSet();
		return factory.getObject();

	}

	@Bean
	public JobRegistry jobRegistry() {
		return (JobRegistry) new MapJobRegistry();
	}

	@Bean
	public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor() {
		JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
		jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry());
		return jobRegistryBeanPostProcessor;
	}

	@Bean
	public SimpleJobOperator jobOperator() {
		SimpleJobOperator jobOperator = new SimpleJobOperator();
		jobOperator.setJobExplorer(getJobExplorer());
		jobOperator.setJobRepository(getJobRepository());
		jobOperator.setJobLauncher(getJobLauncher());
		jobOperator.setJobRegistry((ListableJobLocator) jobRegistry());
		return jobOperator;
	}
	
}
