package com.manman.batch.decider;

import com.manman.common.util.DataShareBean;
import com.manman.common.util.KeyGeneration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BatchDecider implements JobExecutionDecider {
	
	private static final Logger logger = LoggerFactory.getLogger(BatchDecider.class);

	@Autowired
	private DataShareBean<Object> dataShareBean;

	@Autowired
	private KeyGeneration keyGeneration;

	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
		
		logger.debug("BatchDecider jobId : " + jobExecution.getJobParameters().getString("jobId"));
		Boolean isJobExcuteCompleted = (Boolean) this.dataShareBean
				.getData(this.keyGeneration.getKey(stepExecution) + "job");
		
		logger.debug("BatchDecider isJobExcuteCompleted : " + isJobExcuteCompleted);
		
		if (isJobExcuteCompleted != null) {
			
			if (isJobExcuteCompleted.booleanValue()) {
				logger.debug("BatchDecider return endJob");
				return new FlowExecutionStatus("endJob");
			}
			
			if (jobExecution.getJobParameters().getString("jobId") == null) {
				logger.debug("BatchDecider return jobId_null");
				return new FlowExecutionStatus("jobId_null");
			}
			
			this.dataShareBean.putData(this.keyGeneration.getKey(stepExecution) + "job", Boolean.valueOf(true));
			
			return new FlowExecutionStatus(jobExecution.getJobParameters().getString("jobId"));
			
		}
		
		return new FlowExecutionStatus("isJobExcuteCompleted_null");
		
	}
	
}
