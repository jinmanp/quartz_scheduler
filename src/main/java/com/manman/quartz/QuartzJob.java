package com.manman.quartz;

import com.manman.batch.job.BatchJobController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuartzJob implements Job {
	
	private static final Logger logger = LoggerFactory.getLogger(QuartzJob.class);

	@Autowired
	BatchJobController batchJobController;

	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		Trigger trigger = context.getTrigger();
		JobDataMap jobDataMap = trigger.getJobDataMap();
		String jobId = (String) jobDataMap.get("jobId");
		String pgId = (String) jobDataMap.get("pgId");
		
		logger.debug("QuartzJob execute jobId : " + jobId);
		logger.debug("QuartzJob execute pgId : " + pgId);
		
		JobParametersBuilder params = new JobParametersBuilder();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		
		String currentTime = (new SimpleDateFormat("yyyyMMddHHmmss")).format(calendar.getTime());
		String currentYmd = (new SimpleDateFormat("yyyyMMdd")).format(calendar.getTime());
		
		params.addString("currentTime", currentTime);
		params.addString("currentYmd", currentYmd);
		params.addString("jobId", jobId);
		params.addString("pgId", pgId);
		
		batchJobController.runBatchJob(params.toJobParameters());
		
	}
	
}
